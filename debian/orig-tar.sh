#!/bin/sh

set -e

# called by uscan with '--upstream-version' <version> <file>
DIR=pyamf-$2+dfsg
TAR=pyamf_$2+dfsg.orig.tar.gz

# Repack upstream source to tar.gz and clean it
tar zxf $3
mv PyAMF-* $DIR
GZIP=--best tar -cz --owner root --group root --mode a+rX -X debian/orig-tar.excludes -f $TAR $DIR
rm -rf $DIR $3
